const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/webshop');

const productSchema = new mongoose.Schema({
    id: Number,
    name: String,
    desc: String,
    price: Number
});

const Product = mongoose.model('Product', productSchema);

const products = [
    { id: 1, name: "Germinal 1", desc: "description germinal 1", price: 10 },
    { id: 2, name: "Germinal 2", desc: "description germinal 2", price: 20 },
];

Product.insertMany(products)
    .then(() => {
        console.log('Products added successfully');
        mongoose.connection.close();
    })
    .catch(error => {
        console.error('Error adding products:', error);
        mongoose.connection.close();
    });