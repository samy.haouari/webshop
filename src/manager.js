class Product {
    constructor(id = "", name = "", desc = "", price = 0) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.price = price;
    }
}

class Stock {
    constructor() {
        this.list_product = [];
    }

    async init() {
        try {
            const response = await fetch('http://localhost:3000/products');
            const products = await response.json();
            console.log('Products fetched:', products);
            this.list_product = products.map(prod => new Product(prod.id, prod.name, prod.desc, prod.price));
        } catch (error) {
            console.error('Error fetching products:', error);
        }
    }

    get_list_product() {
        return this.list_product;
    }

    get_prod_by_id(id) {
        return this.list_product.find(product => product.id == id) || null;
    }
}

class Cart {
    constructor() {
        this.list_cart = {};
    }

    get_list_cart() {
        return this.list_cart;
    }

    addInCart(id) {
        if (this.list_cart[id]) {
            this.addExistedElem(id);
        } else {
            this.addNew(id);
        }
    }

    removeFromCart(id) {
        if (this.list_cart[id]) {
            if (this.list_cart[id] == 1) {
                delete this.list_cart[id];
            } else {
                this.subExistedElem(id);
            }
        }
    }

    addNew(id) {
        this.list_cart[id] = 1;
    }

    addExistedElem(id) {
        this.list_cart[id]++;
    }

    subExistedElem(id) {
        if (this.list_cart[id] > 0) {
            this.list_cart[id]--;
        }
    }

    get_nbr_product() {
        let total = 0;
        for (const id in this.list_cart) {
            total += this.list_cart[id];
        }
        return total;
    }

    get_total_price(stk) {
        let total = 0;
        for (const id in this.list_cart) {
            const product = stk.get_prod_by_id(id);
            if (product) {
                total += this.list_cart[id] * product.price;
            }
        }
        return total;
    }
}

export { Product, Stock, Cart };
